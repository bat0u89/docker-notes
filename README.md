# docker-notes

## DOCKER COMPOSE, up something locally

### run from the directory that contains the Dockerfile:

cd C:\dev\git_repos\yourproject

### Build the image from the current jar

docker build -t yourproject-dockerimage --build-arg JAR_FILE="target/yourproject-0.0.6-SNAPSHOT.jar" .

#### or ...

docker container create image localstack/localstack

### Run the image to test it:

docker run -i -t yourproject-dockerimage

### Stop image container:

docker stop yourcontainer

### Login to jfrog so you can pull docker images:

docker login yourartifactory.jfrog.io

### Run docker-compose:

cd ..

docker-compose -f docker-compose-local.yml up -d

### Tear down:

docker-compose -f docker-compose-local.yml down

## Enter container:

docker exec -it yourcontainer /bin/bash

## GENERAL bits

### tag image

docker tag whatever_local/yourimage whatever_local/yourimage:1.0

### upload image to artifactory

__First tag it in order to have an alias that is prefixed with the remote host name.__

docker tag whatever_local/yourimage myartifactory.myhost.com/yourimage:1.0

__Then do docker push:__

docker push myartifactory.myhost.com/yourimage:1.0

### DOWNLOAD FILE, from within container

docker cp 5008d3dc4af4:/usr/local/tomcat/${env:TOMCAT_MYAPP_LOGS}/GTM.log %HOME%\Desktop\MYAPP.log
